<?php

// Отправляем правильную кодировку.
header('Content-Type: text/html; charset=UTF-8');

// Выводим все полученные через POST параметры.
// если запрос 2-5) сделан правильно, то можно будет увидеть
// отправленный комментарий в ответе веб-сервера.
print_r($_POST);

print('Привет, мир!');
?>
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <title>Задание 2</title>
</head>

<body>
    <h3>Получить главную страницу методом GET в протоколе HTTP 1.0</h3>
    <img src="1.png">
    <br/>
    <h3>Получить внутреннюю страницу методом GET в протоколе HTTP 1.1</h3>
    <img src="2.png">
    <br/>
    <h3>Определить размер файла file.tar.gz, не скачивая его</h3>
    <img src="3.png">
    <br/>
    <h3>Определить медиатип ресурса /image.png</h3>
    <img src="4.png">
    <br/>
    <h3>Отправить комментарий на сервер по адресу /index.php</h3>
    <img src="5.png">
    <br/>
    <h3>Получить первые 100 байт файла /file.tar.gz</h3>
    <img src="6.png">
    <br/>
    <h3>Определить кодировку ресурса /index.php</h3>
    <img src="7.png">
</body>

</html>